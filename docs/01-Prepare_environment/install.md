---
sidebar_position: 1
---

# Installation of necessary codes

Before starting FWI, users need to install/configure all the required libraries/codes listed below:
- PySpecfem
- SPECFEM_FWI
- ... #TODO: add necessary ones

We have 2 possible ways to

## Manual Install

### PySpecfem

we recommend installing PySpecfem with python3.6 3.7 or 3.8, we have not tested with 3.9.
So we recommend to use some python version control tool e.g. pyenv to use with those versions of Python.
We recommend to use pip in this case the installation is automatic, the dependencies will be automatically installed by pip.
In the pyspecfem repository just type,
```bash
pip install .
```
Some PySpecfem commands will be installed, including :
```
pyspecfem_setup
pyseseiswaves
pyspecfem_create_initial_smooth_model
pyspecfem_make_report_on_event_all_traces
pyspecfem_compute_pert_xdmf
Their use will be described in the following.
```
### SPECFEM FWI
 The installation of specfem_fwi requires fortran, c and nvcc compilers for GPU computing and a MPI library.
 A compiler help script is provided with specfem_fwi to configure the different compilers.
 It is necessary to inform the name of the various compilers and possibly the paths to the cuda and MPI libraries if the system does not have information on them.
 In addition, FFTW and hdf5 can be used, in which case the corresponding variables must be entered.
 Here is an example of the compilation script:

```bash
#!/bin/bash
####### CPU #######
fc=ifort
cc=icc
mpifc=mpif90

# mpi include diectory
mpi_inc=/usr/lib/x86_64-linux-gnu/openmpi/include

##### FFTW #######
FFTW_DIR=/usr/local
#or
FFTW_INC=
FFTW_LIB=

#### HDF5 #####
#-with-hdf5
mpif90=h5pfc

#####  GPU ######
# for compiling wih cuda specify the following option with the cuda flag decribed below

# nvidia GPU option
#
cuda_flag=cuda10

#   our options for compiling in differents GPU nvidia architecture :
#
# cuda10 Turing RTX500, RTX4000, RTX***
# cuda9  Volta V100,
# cuda8  Pascal P100, P***
# cuda(5 or 6)  Kepler K80
# cuda5 kepler K20
#

# configure to create Makefile
./configure  --with-mpi CC=$cc FC=$fc MPIFC=$mpifc MPI_INC=$mpi_inc --with-fftw3-dir=$FFTW_DIR --with-cuda=$cuda_flag --with-hdf5

# compile the full package
make all
```

## Install with Docker
