# Appendix A

## A1. setup file

An example of setup file is given below. Line with `#` is a comment. 
The file contains keywords and values separated by a `“:”`

```text
###########################################################                                                               
#
#                    Domain for specfem                              
#               meshing Chunk with meshfem                      #
###########################################################

# use coupling specfem and axisem
axisem_coupling : 1
mesh_chunk : 1
use_new_script_axisem : 1
model_type : ak135
ngnod : 27

# -----------------------------------------------------
#  Domain in spherical coordiantes
#  since axisem use geocentric coordiantes
#  we assume that the following values are
# given in geocentric coordinates.

# center of domain lat, lon  (decimal degrees)
LONGITUDE_CENTER_OF_CHUNK : 136.
LATITUDE_CENTER_OF_CHUNK :  37.5

# extention of chunk before rotation (decimal degrees)
LONGITUDE_EXTENTION : 24
LATITUDE_EXTENTION : 15.5

# vertical size (km)
VERTICAL_EXTENTION : 900

# rotation of domain with respect to z axis in the center  # of domain given with respect to the north 
# (0. means no rotation)
AZIMUTH_OF_CHUNK : -50

# depth for buried box (km)
# set 0. for domain that reach the free surface of the  
# earth
DEPTH_OF_CHUNK : 0.


#----------------------------------------------------------
# topography file path  (comment to unuse it)
# pyspecfem will automatically download ETOPO1 or 
# SRTM15_PLUS file from NOAA server if not found at the 
# specified path.
ETOPO1 : home/topo/etopo1_bed_g_f4.flt

#----------------------------------------------------------
# discretization of domain in km
SIZE_EL_LONGITUDE_IN_KM : 20.0
SIZE_EL_LATITUDE_IN_KM  : 20.0

# depth
SIZE_EL_DEPTH_IN_KM : 20.0

# doubling 
#number_of_doubling : 1
#depth_of_doubling : -47.5

# tripling
number_of_tripling : 1
depth_of_tripling : -55. 

###########################################################                                                            #
#                    AXISEM CONFIG                              #
###########################################################

# background model used in axisem : ak135, iasp91, prem
EARTH_MODEL : ak135

# Dominant period [s]
DOMINANT_PERIOD : 8

# MPI decomposition for AXISEM 
NTHETA_SLICES :  20
NRADIAL_SLICES : 2
 
SOURCE_FUNCTION : errorf

###########################################################                                                         #
#                 SOURCES - RECEIVERS                                                                                       #
###########################################################

SIMULATION_TYPE : moment

input_directory : fwi_input_files

# path to station file (format :
# station_name  network_name latitue(deg) longitude(deg) 
# elevation(m) buried(m)
station_type : geo_file

###########################################################                                                          #
#                   SPECFEM CONFIG                              #
###########################################################

# MPI decomposition for SPECFEM
number_of_mpi_domain : 4

# simultanteous runs
number_of_simultaneous_runs : 1

# simulation time sampling
time_length : 500
time_step : 0.025
time_step_data : 0.05

###########################################################
#                                                               
#                   COUPLING CONFIG                             #
###########################################################


mpi_process_coupling : 80

###########################################################                                                                                                                  #
#                       PATHS                                                                                               #
###########################################################

# PATH TO SPECFEM DIRECTORY
specfem_directory : /path/to/specfem_fwi
AXISEM_COUPLING_DIR : /path/to/specfem_fwi/src/extern_packages/coupling/

###########################################################
#
#                       SCRIPTS                                 #
###########################################################
script_type : slurm
use_inverse_problem : 1
use_gpu : 1
use_input_fwi : 1
hdf5_enabled : 0
number_of_io_node : 0
```


## A2. inversion_fwi_par file

The inversion_fwi_par file contains all the parameter that control the inversion, like stopping criteria, smoothing, damping, output controls.
Frequency band to use in inversion. 
Below, this is an example of such file with line begins with `“#”` are comments and the other lines are composed by a key and value separated by `“:”`

```text
############# stopping criteria ##########################
Niter : 25            # maximum FWI iteration allowed 
relat_grad : 1e-5     # relative decrease on gradient 
relat_cost : 1e-5     # relative decrease on cost function

##########   cost function  ##############################
prior_data_std : 0.001    # estimated error on data
normalize_data :  .true.  # normalize data by maximum of
                          # the traces
max_relative_pert : 0.9   # maximum relative perturbation
                          # allowed for line search

######## parameter to invert #############################
# choice : ISO=(rho, vp, vs), VTI=(rho, vp, vs, ep, gm, de) 
param_family : ISO 
nb_inver : 3  
param_to_inv : rho vp vs 

# damping of each parameter in [0. 1.] (1 => allows 100%  
# variation)
normalize_inv_parameter : 0.1 0.1 0.1
# smoohting length on model (meters)  
laplac_filter : 150000. 150000. 150000.

#############  Band frequency #############################
use_frequency_band_pass_filter : 2
fl : 0.04  0.04  # min frequency Hz
fh : 0.1   0.12  # max frequensy Hz

########### PRECOND  #####################################
# gradient=0 outisde domain : xmin, xmax, ymin, ymax, zmin,
# zmax defined below
taper : -1334339. 1334339. -861760.7 861760.7 -1135664.  0. 

############ I/O options #################################
input_fd_model : .true. # read input model on regular grid 
output_model : .true. # write ouput model on the spectral
                      # element mesh

############ save some fields at each iteration ##########
dump_model_at_each_iteration : .true.
dump_gradient_at_each_iteration : .true.
dump_descent_direction_at_each_iteration : .true.
```



