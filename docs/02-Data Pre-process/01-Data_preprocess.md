# Data preprocess

The preprocessing of the seismic data is to select the traces and to eliminate noisy traces that could pollute the inversion. The traces from an event are stored in a separate directory which contains all the traces and the information about the seismic source. For this purpose, the user can refer to the well-known obspy (https://docs.obspy.org/) package to download traces and event information.


## Data preparation

The seismological data can be stored in two formats sac (https://ds.iris.edu/files/sac-manual/manual/file_format.html ) or miniseed (https://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/).  A GUI tool, pyseiswaves, is used to select the data and save the resulting traces in the format used by specfem_fwi.

## File format

The following files are produced by pyseiswaves and are needed for the inversion step based on specfem_fwi.

``` text
data.bin : binary file which contains the data
data.hdr : meta data for data.bin
STATIONS_geogr : stations information (network, name, position)
STATIONS_weight : weight on each trace (0 => not used, 1=> used)
STATIONS_BAZ : back azimuth for each stations
pick.txt : arrival time of the seismological phase of interest (eg: P, S)
window.txt : time window to invert with respect to the pick
CMTSOLUTON : information on earthquake (position, moment tensor)
```

After this first step, all the data are put inside a directory called input_files_fwi/ which contains one directory per event and each directory contains the previous files :

![png](preproc_0.png)


## pyseiswave usage

### Displaying data

Launch pyseiswave by just typing in comand teminal.
Reading data by using menu tool bar
`“File”→”Open Mseed/SAC Data”`
Then select the directory with data and select file format.

![png](preproc_1.png)

After loading all data, the traces are displayed in the “Seismogram” tab.

![png](preproc_2.png)


At the right the “Filter Seismogram” panel allows to filter the data and display with different gain.

A mouse click on a trace will select it for discarding, the trace then appears in grey. A first visual inspection of data allows the user to select and remove a spurious data by a simple mouse click.


### Quality control metric

For the seismic tomography purpose, the data must be carefully cleaned for this purpose  after a first visual inspection, some quality control metric can be used to help find and remove the remaining bad data. First focus on phase of interest by checking in `“Display Seismograms”` panel : `“Phase”, “Phase Ref time”`, `“Remove Stations”`, `“Station Name”`.  

![png](preproc_3.png)

The theoretical arrival time of phase of interest (P teleseismic in the figure) is then plotted in each traces. A mouse scroll allow to zoom on the phase.

To compute the quality control metric, check “Data window selection” in “Window for residual and wavelet” panel and select the window by adjusting the selector tool that appears in the “Seismograms” tab.

![png](preproc_4.png)

In processing panel select `“Data CC Align/stack pick”`, and push `“Apply”` button. A new figure is then displayed with the stack of all traces in the selected window. Move the vertical bar with mouse to select a reference time in the stacked wavelet and the push the `“Set Pick”` button. The tool will process some metric computation then by pushing the button `“Display network”` and `“Display map”` in the `“Display Residual Map”` panel. The map will be updated, each station position appears as a coloured point. This allow a visual inspection and some spurious colours, (eg : a blue spot in red patch, or red spot in blue patch) need to be inspected by clicking on the point. Then the corresponding trace is displayed at the bottom of `“Seismograms”` tab. The user can make a decision to remove or keep this data.  

![png](preproc_5.png)

Four metrics are available, phase residual, amplitude residual, correlation and signal on noise ratio. In the following figures the phase residuals are displayed at the right and amplitude misfit at the left.  

|          |           |
| ---      | ---       |
| ![png](preproc_6.png) | ![png](preproc_7.png) |

### Saving input data for specfem_fwi

After cleaning data set unselect in “Display Seismograms” panel “Phase Ref time”. In Processing panel select “–-” and click on “Apply” button. Then select the time window to cut the data for the seismic tomography.  

![png](preproc_8.png)

For saving the selected data set, click in the menu bar:  
`“File”`→ `“Save Data (binary)”`   
All the files needed for specfem_fwi will be writen in the chosen directory. After processing all events the data are ready for the first step of the inversion.

