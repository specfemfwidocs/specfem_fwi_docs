# Seismic Tomography

This section describe how to launch the inversion process. The first step is to upload the data on the cluster and prepare the setup file.  This setup file is described in appendix A1. To prepare all the files and scripts for the process use the following command: 

```bash
pyspecfem_setup setup_file.in
```
where setup_file.in is the name of the setup file used for the case.  


## Synthetics computation

### Preparing initial model

The initial guess model for seismic tomography is a smoothed version of a layered classical seismological model (eg PREM, IASP91, AK135, …) to produce it, type this command :   

```bash
pyspecfem_create_initial_smooth_model –options 
```

where options can be defined by the user. The options available are the following (use –help ): 

``` text
usage: pyspecfem_create_initial_smooth_model [-h] 
[--topo_file TOPO_FILE]                               
[--topo_type TOPO_TYPE]                               
[--lon_center LON_CENTER]                                
[--lat_center LAT_CENTER]                                
[--azi AZI]                               
[--lon_extension LON_EXTENSION]                              
[--lat_extension LAT_EXTENSION]                             
[--interval INTERVAL]                              
[--filter_size FILTER_SIZE]                         
[--nzkeep NZKEEP]

Create input smooth model on regular grid

optional arguments:
  -h, --help   show this help message and exit
  --topo_file TOPO_FILE
file with topography [/gpfswork/rech/gkj/rukn006/topo/etopo1_bed_g_f4.flt]
  --topo_type TOPO_TYPE
type of file with topography [etopo1]
  --lon_center LON_CENTER
longitude center of chunk [-3.]
  --lat_center LAT_CENTER
latitude center of chunk [39.]
  --azi AZI             
azimuth of chunk [0.]
  --lon_extension LON_EXTENSION
extension of chunk in longitude [16.5]
  --lat_extension LAT_EXTENSION
extension of chunk in latitude [23.5]
  --interval INTERVAL
size of the grid size in meters [10000.]
  --filter_size FILTER_SIZE 
the number of points which is used for smoothing [40]
  --nzkeep NZKEEP 
number of the elements from the surface which keeps the value without smoothing [5]
```

for example, 

```bash
pyspecfem_create_initial_smooth_model --lon_center 136.  --lat_center 37.5 --azi " -50." --lon_extension 24. --lat_extension 15.5
```

This command produces 4 files which contains all informations for the initial guess model  :

```text
fd_grid.txt    : desciption of the regular grid 
rho_smooth.bin : values of density
vp_smooth.bin  : values of P-wave velocity
vs_smooth.bin  : values of S-wave velocity
```

### Creating the mesh

To create the mesh a script which launch all the specfem_fwi meshers tools is used by typing: 

```bash
./launch_mesher.sh
```
This script schedule the xmeshfem3D code and xgenerate_databases using the scheduler installed in the cluster. 

### forward simulation

The forward simulation is simply launched by the command :

```bash
sbatch run_FWI_forward.batch
```
After the forward simulation run, for each event synthetics are computed and stored in same directory where are the data in a binary file :  `synth_smooth_ak135.bin`, with the same format than data. 


## Source time function estimation

The source estimation is the first step of the inversion when the model is remain fixed and only the source time function of the event is estimated by a least square inversion comparing the data and previously computed synthetics. The psyseiswave GUI is used to perform this step.  In the menu toolbar, 

`“File”` → `“Load binary data”`

then select binary file data and the corresponding meta data file with *.hdr extension. After the complete loading of data, return in menu toolbar to load synthetics,

`“File”` → `“Load binary Synthetics”`

then select synthetics and the correspondig meta data file with *.hdr extension.

In the “Seismograms” tab the data are displayed and the synthetics are in the “Synthetics” tab.

Now select a time window around the phase of interest on data, for that select first in `“Display Seismograms”` panel : `“Phase”`, `“Phase Ref time”`, `“Remove Stations”`, `“Station Name”`. 
Then select the window with the interactive data selection control.

![png](./tomo_0.png)

Now the gui is ready to compute the source time function, to do that select `“Wavelet”` in  `“Processing panel”` and push the `“Apply”` button. 
The source time function obtained is displayed in the `“Wavelet”` tab and synthetics convolved with the wavelet and data are displayed together in the the `“Wavelet Synthetics”` tab. 
The data appears in red and synthetics in white.
To save the source time function click on menu toolbar,
`“File”` → `“Save Wavelet”`
The wavelet is written in `wavelet.txt` file in the event directory. 

![png](./tomo_1.png)

## Full Waveform Inversion

### Launching inversion

The purpose of full waveform inversion is now to minimize the misfit between the data and synthetics by modifying the model. 
Before launching the inversion, the wavelet files computed before must be uploaded in event directory where the data are stored on the HPC cluster. 
The file inversion_fwi_par described in `appendix A.2` is used to tune the different parameters involved in the inversion.
The important parameters that have an impact on the solution are the frequency band used and the smoothing. 
It is recommended to start at the lowest possible frequency (0.1 Hz for the teleseismic case) and to increase the frequency gradually, 0.12, 0.15, 0.2 Hz.
To do this, simply fill in the fields in the file inversion_fwi_par with several frequency bands.
It is recommended to start by smoothing much more than 100 km and when the model is more accurate, this smoothing can be reduced by a factor of 2 or more depending on the level of noise in the data.
The noisier the data, the larger the smoothing should be.  


### Monitoring iterations

The cost function during iterations is stored in the file `output_iteration.txt`.  

```text
iter | cost function | gradient norm | relat cost |  relat grad| nb line search
   FREQUENCY GROUP :  3.9999999E-02  0.1000000    
 
    0| 0.31801636E+04| 0.32137892E+10| 0.10000E+01| 0.10000E+01|
    1| 0.29109644E+04| 0.16411451E+10| 0.91535E+00| 0.51066E+00|  1
    2| 0.26785173E+04| 0.57882547E+09| 0.84226E+00| 0.18011E+00|  1
    3| 0.25527654E+04| 0.22598426E+09| 0.80272E+00| 0.70317E-01|  1
    4| 0.24700425E+04| 0.12514536E+09| 0.77670E+00| 0.38940E-01|  1
    5| 0.24021162E+04| 0.10127956E+09| 0.75534E+00| 0.31514E-01|  1
    6| 0.23394854E+04| 0.86354064E+08| 0.73565E+00| 0.26870E-01|  1
    7| 0.22826199E+04| 0.55500520E+08| 0.71777E+00| 0.17269E-01|  1
    8| 0.22333950E+04| 0.41608764E+08| 0.70229E+00| 0.12947E-01|  1
    9| 0.21949937E+04| 0.52968624E+08| 0.69021E+00| 0.16482E-01|  1
   10| 0.21740005E+04| 0.30048956E+08| 0.68361E+00| 0.93500E-02|  1
   11| 0.21537427E+04| 0.18997396E+08| 0.67724E+00| 0.59112E-02|  1
   12| 0.21286589E+04| 0.15205215E+08| 0.66936E+00| 0.47312E-02|  1
   13| 0.21027058E+04| 0.14745280E+08| 0.66119E+00| 0.45881E-02|  1
   14| 0.20775898E+04| 0.30293150E+08| 0.65330E+00| 0.94260E-02|  1
   15| 0.20580979E+04| 0.16897050E+08| 0.64717E+00| 0.52577E-02|  1
   16| 0.20448235E+04| 0.12351874E+08| 0.64299E+00| 0.38434E-02|  1
   17| 0.20261509E+04| 0.79018350E+07| 0.63712E+00| 0.24587E-02|  1
   18| 0.20041323E+04| 0.55027610E+07| 0.63020E+00| 0.17122E-02|  1
   19| 0.19845096E+04| 0.12265880E+08| 0.62403E+00| 0.38166E-02|  1
   20| 0.19726245E+04| 0.97775460E+07| 0.62029E+00| 0.30424E-02|  1
   21| 0.19592942E+04| 0.82504650E+07| 0.61610E+00| 0.25672E-02|  1
   22| 0.19429418E+04| 0.13943988E+08| 0.61096E+00| 0.43388E-02|  1
   23| 0.19287456E+04| 0.69805965E+07| 0.60649E+00| 0.21721E-02|  1
   24| 0.19186587E+04| 0.51471280E+07| 0.60332E+00| 0.16016E-02|  1
   25| 0.19051600E+04| 0.43084080E+07| 0.59908E+00| 0.13406E-02|  1
   26| 0.18891104E+04| 0.48651890E+07| 0.59403E+00| 0.15138E-02|  1
 
   FREQUENCY GROUP :  3.9999999E-02  0.1200000    
 
    0| 0.16941924E+04| 0.10263220E+08| 0.10000E+01| 0.10000E+01|
    1| 0.16907869E+04| 0.18757286E+08| 0.99799E+00| 0.18276E+01|  5
    2| 0.16868927E+04| 0.11067442E+08| 0.99569E+00| 0.10784E+01|  5
    3| 0.16833455E+04| 0.41378188E+07| 0.99360E+00| 0.40317E+00|  1
    4| 0.16773461E+04| 0.13777090E+08| 0.99006E+00| 0.13424E+01|  1
    5| 0.16703187E+04| 0.13780261E+08| 0.98591E+00| 0.13427E+01|  1
    6| 0.16656694E+04| 0.38308768E+08| 0.98316E+00| 0.37326E+01|  1
    7| 0.16620652E+04| 0.16377900E+08| 0.98104E+00| 0.15958E+01|  1
    8| 0.16592399E+04| 0.21453802E+08| 0.97937E+00| 0.20904E+01|  1
    9| 0.16581714E+04| 0.17201162E+08| 0.97874E+00| 0.16760E+01|  1
   10| 0.16560343E+04| 0.18342256E+08| 0.97748E+00| 0.17872E+01|  1
   11| 0.16555096E+04| 0.17983568E+08| 0.97717E+00| 0.17522E+01|  1
   12| 0.16537134E+04| 0.18645348E+08| 0.97611E+00| 0.18167E+01|  1
   13| 0.16536510E+04| 0.20093512E+08| 0.97607E+00| 0.19578E+01|  1
   14| 0.16522853E+04| 0.21830480E+08| 0.97526E+00| 0.21271E+01|  1
   15| 0.16468512E+04| 0.95345762E+06| 0.97206E+00| 0.92900E-01|  2
   16| 0.16448369E+04| 0.96949056E+06| 0.97087E+00| 0.94463E-01|  2
   17| 0.16398049E+04| 0.15627898E+08| 0.96790E+00| 0.15227E+01|  1
   18| 0.16283970E+04| 0.12537640E+08| 0.96116E+00| 0.12216E+01|  3
   19| 0.16207343E+04| 0.42303805E+07| 0.95664E+00| 0.41219E+00|  1
   20| 0.16170309E+04| 0.19001239E+07| 0.95446E+00| 0.18514E+00|  1
   21| 0.16130270E+04| 0.95427300E+06| 0.95209E+00| 0.92980E-01|  1
   22| 0.16083943E+04| 0.13204686E+07| 0.94936E+00| 0.12866E+00|  1
   23| 0.16034520E+04| 0.37238735E+07| 0.94644E+00| 0.36284E+00|  1
   24| 0.15980964E+04| 0.42648505E+07| 0.94328E+00| 0.41555E+00|  1
   25| 0.15897390E+04| 0.54420290E+07| 0.93835E+00| 0.53025E+00|  1
   26| 0.15828480E+04| 0.17829610E+08| 0.93428E+00| 0.17372E+01|  1
```
The left-hand column is the iteration number, then comes the value of the cost function, the norm of the gradient and the relative values of the cost and the norm of the gradient.  
