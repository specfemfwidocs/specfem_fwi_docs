---
sidebar_position: 0
---

# Introduction 

#TODO: write here the brief explanation of this project
Rough explanation on the entire workflow
and introduction for each subchapters

SPECFEM FWI toolkit is a set of libraries for facilitating a Full Wave Inversion (FWI) caluculation with using SPECFEM3D, one of the most matured and efficient numerical computation codes for simulating a wave propagation with spectral element method, especially for running on high performance computers.


## Workflow for FWI

Below is an example workflow for FWI.

#TODO: add flowchart
- define target
- data correction
- preprocess for preparing observation dataset with a good quality
- run one single forward then calculate the source wavelet for each event
- prepare initial model
- run FWI calculation
- check the result
- iterate with higher frequency bounds

The datails of each steps will be descibed in the following sections.


## Utility library for pre/post processing **PySpecfem**

PySpecfem is a utility software which helps a FWI calculation widely from dataset preparation to post processings.

## The computation core **SPECFEM_FWI**

As the core of the computation for making a FWI calculation, we newly developed SPECFEM_FWI, which is the extended implementation of SPECFEM3D cartecian for carrying out a FWI tomography using L-BFGS optimization algorithm.
For the details of this implementation, please refer [V. Montailler 20**](#TODO: add reference link).

## Reference

Please refer the articles below when you will use this toolkit as a part of your research.
#TODO: add CHEESE reference/SPECFEM reference 
- a 
- b


## Installation
Please read the section [Installation](./01-Prepare_environment/install.md) for getting your computer environment ready for FWI.