# Post-process

This section describe how to visualize the inverted model and how to control the data fit.

## Data fit 

The adjustment of the data can be controlled using a pyspecfem command 

```bash
pyspecfem_make_report_on_event_all_traces
```

The option `–help` display all the options available. 
For example to produce a report  on data fit, 

```bash
pyspecfem_make_report_on_event_all_traces --event_directory GCMT_event_CELEBES_SEA_Mag_7.3_2017-1-10-6/ --fname_syn data_for_fwi.bin1030_dir --use_FWI_window --frq_max 0.1
```

The result is a pdf file (`event_report_all_traces.pdf`) that contains the geometry network, the data fit by plotting each trace for a given component and quality metric maps.  The following figure are extracted form the report produced, the synthetics traces (red) are compared with the data (black) in initial model :

![png](postpro_0.png)

and in the final model, with a better agreement between synthetics and data:

![png](postpro_1.png)

Also a map displaying residuals, amplitude misfit and correlation in initial model : 

![png](postpro_2.png)

And for the final model :

![png](postpro_3.png)

Thus the metrics are improved by the inversion and the resulting model explains the data much better than the original model.


## Model visualization

The results of each iteration are stored in the VISU directory. For example the file `VISU/model__iter_20026.xdmf` means that the model corresponds to iteration 26 of frequency band 2.  To view the model, simply open this file with the open source code [Paraview](https://www.paraview.org/). 


### visualization in specfem_fwi mesh

To visualize the perturbations of the model with respect to the reference model, it is necessary to use a utility that will calculate this difference (in %) and store it in the file VISU/pert_model.xdmf. 
For example the command,

```bash
pyspecfem_compute_pert_xdmf –cur model__iter_20026.xdmf
```

computes the perturbations of the iteration 26 model in the frequency band 2 and the visualization with paraview can be done by opening the file `pert_model.xdmf`.
The paraview code allows to make 3D visualization, make slices in the model as on the following figures and also allows to plot the stations (green dots) and also the seismicity (white dots) as well as the coastlines (in black). 

![png](postpro_4.png)
![png](postpro_5.png)


### Other output format

We have other options for output file formats, when the `hdf5_enabled` flag in the setup file is set to `1`, all output merges to hdf5 which allows for better portability. 
In addition we can also use a regular grid for model output rather than the spectral element grid. 
In this case we need to set the `ouput_fd_model` flag in the `inversion_fwi_par` file to `.true.`



