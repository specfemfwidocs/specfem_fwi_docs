import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Very efficient and fast FWI code for HPCs',
    Svg: require('../../static/img/specfem3d.svg').default,
    description: (
      <>
        SPECFEM FWI is a fast and efficient FWI code thanks to the use of SPECFEM3D, which is one of the most optimized codes for HPC and GPU machines for seismic full wave modeling based on the spectral element method.
        This code is designed to use on a Exascale HPC machine. The scalability of the solver is almost perfect. Asynchronous file I/O is implemented in modern way (I/O servers) to break an I/O bottleneck.
      </>
    ),
  },
  {
    title: 'Python and terminal command interface for easy setup and running of FWI calculations',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        PySpecfem is a python package that provides a simple interface to setup and run SPECFEM FWI simulations.
        It is designed to be used in a terminal or in a python script/jupyter notebook.
        Useful functions for Pre/Mid/Post processings are also provided.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
