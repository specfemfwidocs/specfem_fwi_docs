/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Full wave inversion with SPECFEM3D',
  tagline: 'Documentation for the Specfem Fwi and utilities.',
  url: 'https://specfemfwidocs.gitlab.io',
  baseUrl: '/specfem_fwi_docs/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'mnagaso', // Usually your GitHub org/user name.
  projectName: 'specfem_fwi_docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Specfem Fwi',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Docs',
        },
        {to: 'examples', label: 'Examples', position: 'left'},
        //{to: '/blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/vmont/specfem_fwi',
          label: 'Specfem FWI',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/vmont/pyspecfem',
          label: 'PySpecfem',
          position: 'right',
        },

      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Docs',
              to: '/docs/intro',
            },
          ],
        },
        //{
        //  title: 'Community',
        //  items: [
        //    {
        //      label: 'Stack Overflow',
        //      href: 'https://stackoverflow.com/questions/tagged/docusaurus',
        //    },
        //    {
        //      label: 'Discord',
        //      href: 'https://discordapp.com/invite/docusaurus',
        //    },
        //    {
        //      label: 'Twitter',
        //      href: 'https://twitter.com/docusaurus',
        //    },
        //  ],
        //},
        {
          title: 'More',
          items: [
            //{
            //  label: 'Blog',
            //  to: '/blog',
            //},
            {
              label: 'CNRS LMA',
              href: 'http://www.lma.cnrs-mrs.fr/?lang=en',
            },
            {
              label: 'ChEESE',
              href: 'https://cheese-coe.eu/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} SPECFEM FWI Project.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/specfemfwidocs/specfem_fwi_docs/docs/',
        },
        //blog: {
        //  showReadingTime: true,
        //  // Please change this to your repo.
        //  editUrl:
        //    'https://gitlab.com/specfemfwidocs/specfem_fwi_docs/examples/',
        //},
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
