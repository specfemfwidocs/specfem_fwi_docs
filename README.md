# SPECFEM FWI documentation

This is a repository dedicated for deploying SPECFEM FWI documentation.  
This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

For modifying the Docs section, please add a directory or markdown file to the `docs` directory.  
For adding an example, please add a directory or markdown file to the `src/pages/examples` directory.
Other static page may be added to the `src/pages` directory.  

## prerequisites
- nodejs
- yarn

## Installation of necessary node modules

```console
yarn install
```

## Local Development

```console
yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

## Build

```console
yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Deployment
After commiting any changes to the repository, you can deploy the website to gitlab pages by

```console
git push
```

then you may access the website at [https://specfemfwidocs.gitlab.io/specfem_fwi_docs/](https://specfemfwidocs.gitlab.io/specfem_fwi_docs/)